const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    desc: {
        type: String,
        max: 500
    },
    userId: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    likes: {
        type: Array,
        default: []
    }

}, {timestamps: true});

module.exports = mongoose.model('Post', postSchema);