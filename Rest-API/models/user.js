const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 3,
        max: 20
    },
    email: {
        type: String,
        required: true,
        unique: true,
        min: 7,
        max: 30
    },
    password: {
        type: String,
        required: true,
        min: 6
    },
    profilePicture: {
        type: String,
        default: ""
    },
    coverPhoto: {
        type: String,
        default: ""
    },
    followers: {
        type: Array,
        default: []
    },
    followings:{
        type: Array,
        default: []
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    from:{
        type: String,
        max: 50
    },
    city: {
        type: String,
        max: 50
    },
    desc: {
        type: String,
        max: 50
    },
    relationship: {
        type: Number,
        enum: [1,2,3]
    }
}, {timestamps: true});

module.exports = mongoose.model('User', userSchema);