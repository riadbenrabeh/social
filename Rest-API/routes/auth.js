const route = require('express').Router();
const bcrypt = require('bcrypt');
const User = require('../models/user');


//Register
route.post('/register', async (req, res, next) => {
    const password = req.body.password;
    const name = req.body.name;
    const email = req.body.email;
    console.log({password, name, email})
    try {
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);
        const user = new User({
            name,
            email,
            password: hashedPassword
        });
        const result = await user.save();
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json(err);
    }
});

//Login

route.post('/login', async (req, res, next) => {
    try {
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            res.status(404).json('user not found');
        }
        const passwordIsValid = await bcrypt.compare(req.body.password, user.password);
        if (!passwordIsValid) {
            res.status(401).json('password incorrect.');
        }

        res.status(200).json(user);
    } catch(err) {
        console.log(err);
        res.status(500).json('internal error');
    }
})

module.exports = route;