const route = require('express').Router();
const User = require('../models/user');
const Post = require('../models/posts');

//create a post
/**
 * POST /api/posts
 */
route.post('/', async (req, res, next) => {
    if (!req.body.userId) {
        return res.status(403).json('you can not create a new post.');
    }

    try {
        const user = await User.findById(req.body.userId);
        if (!user) {
            return res.status(404).json('user not found.');
        }

        const post = new Post({
            userId: req.body.userId,
            desc: req.body.desc,
            image: req.body.image
        });
        const savedPost = await post.save();
        res.status(200).json('post created successfully.');
    } catch (err) { return res.status(500).json('internal error') }

});

//update a post
/**
 * PUT /api/posts/:id
 */

route.put('/', async (req, res, next) => {
    try {
        const user = await User.findById(req.body.userId);
        const post = await Post.findById(req.body.postId);
        if (!user) {
            return res.status(404).json('user not found.');
        }

        if (!post) {
            return res.status(404).json('post not found.');
        }

        if (post.userId !== req.body.userId) {
            return res.status(403).json('impossible to edit this post.');
        }

        await post.updateOne({ $set: req.body });
        res.status(201).json('post has ben updated successfully.');
    } catch (err) {
        console.log(err);
        return res.status(500).json('internal error.')
    }
});

//delete a post
/**
 * DELETE /api/posts/:id
 */

route.delete('/:id', async (req, res, next) => {
    try {
        const user = await User.findById(req.body.userId);
        const post = await Post.findById(req.params.id);
        if (!user) {
            return res.status(404).json('user not found.');
        }

        if (!post) {
            return res.status(404).json('post not found.');
        }

        if (post.userId !== user._id.toString()) {
            return res.status(403).json('Not allowed to delete a post that are not yours.');
        }

        await post.deleteOne();
        res.status(201).json('post successfully deleted.');

    } catch (err) {
        console.log(err);
        return res.status(500).json('server error.');
    }
})
//like or dislike a post
/**
 * PUT /api/posts/:id/like
 */

route.put('/:id/like', async (req, res, next) => {
    try {
        const post = await Post.findById(req.params.id);
        const user = await User.findById(req.body.userId);

        if (!post) {
            return res.status(404).json('post not found');
        }

        if (!user) {
            return res.status(404).json('user not found');
        }

        if (post.likes.includes(req.body.userId)) {
            await post.updateOne({ $pull: { likes: req.body.userId } });
            return res.status(200).json('post has been disliked.');
        } else {
            await post.updateOne({ $push: { likes: req.body.userId } });
            return res.status(200).json('post has been liked.');
        }
    } catch (err) {
        console.log(err);
        return res.status(500).json('server error.');
    }
});

// get a post
/**
 * GET /api/posts/:id
 */
route.get('/:id', async (req, res, next) => {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            return res.status(404).json('post not found.');
        }
        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json('server error');
    }
});

//get all timeline posts
/**
 * GET /api/posts/timeline/:userId
 */

route.get('/timeline/:userId', async (req, res, next) => {
    if (!req.params.userId) {
        return res.status(403).json('something went wrong.');
    }

    try {
        const user = await User.findById(req.params.userId);
        if (!user) {
            return res.status(404).json('user not found.');
        }

        const userPosts = await Post.find({ userId: req.params.userId })
        const friendsPosts = await Promise.all(
            user.followings.map(friendId => {
                return Post.find({ userId: friendId });
            })
        );

        res.status(200).json(userPosts.concat(...friendsPosts));
    } catch (err) {
        console.log(err);
        return res.status(500).json('server error');
    }
});

/**
 *  GET user's all posts
 * GET /api/posts/profile/:username
 */

route.get('/profile/:username', async (req, res, next) => {
    try {
        const user = await User.findOne({ name: req.params.username });
        if (!user) {
            return res.status(404).json('user not found.');
        }

        const posts = await Post.find({ userId: user._id });
        res.status(200).json(posts);
    } catch (err) {
        console.log(err);
        return res.status(500).json('server error');
    }
})

module.exports = route;

