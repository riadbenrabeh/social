const route = require('express').Router();
const bcrypt = require('bcrypt');
const User = require('../models/user');



//find and update a user by id 
route.put('/:id', async (req, res, next) => {
    if (req.body.userId.toString() === userToUnfollowId || req.body.isAdmin) {
        if (req.body.password) {
            try {
                const salt = await bcrypt.genSalt(10);
                req.body.password = await bcrypt.hash(req.body.password, salt);
            } catch (err) {
                return res.status(500).json(err);
            }
        }
        try {
            await User.findByIdAndUpdate(userToUnfollowId, {
                $set: req.body
            });
            res.status(201).json('successfully updated.');
        } catch (err) {
            return res.status(500).json(err);
        }

    } else {
        return res.status(403).json('impossible to update this profile');
    }
})
//get a user by id
route.get('/:id', async (req, res, next) => {
    try {
        const user = await User.findById(req.params.id);
        const { updatedAt, createdAt, password, ...other } = user._doc;
        if (user) {
            res.status(200).json(other);
        } else {
            res.status(404).json('not found');
        }
    } catch (err) {
        return res.status(500).json(err);
    }

});

//get a user by name

route.get('/', async (req, res, next) => {
    try {
        console.log('FROM GET');
        const user = await User.findOne({ name: req.query.userName });
        console.log(req.query.userName);
        const { updatedAt, createdAt, password, ...other } = user._doc;
        console.log('USER : ', user)
        if (user) {
            res.status(200).json(other);
        } else {
            res.status(404).json('not found');
        }
    } catch (err) {
        return res.status(500).json(err);
    }

});


/**
 * GET all friends
 * GET api/users/friends/:userId
 */

route.get('/friends/:userId', async (req, res, next) => {
    try {
        const user = await User.findById(req.params.userId);
        if(!user) {
            return res.status(404).json('user not found.');
        }
        const friends = await Promise.all(
            user.followings.map( friendId => User.findById(friendId))
        );

        const friendsList = [];
        friends.map((friend) => {
            const {name, profilePicture, _id} = friend;
            friendsList.push({name, profilePicture, _id});
        })

        res.status(200).json(friendsList);
    } catch (err) {
        return res.status(500).json('Server error.');
    }
})
//delete a user

route.delete('/:id', async (req, res, next) => {
    if (req.body.userId === userToUnfollowId || req.body.isAdmin) {
        try {
            await User.deleteOne({ _id: req.body.userId });
            // should delete all things related to it smartly
            res.status(201).json('account deleted successfully');
        } catch (err) {
            return res.status(500).json(err);
        }
    } else {
        return res.status(403).json('You cant delete this account.')
    }
})

// follow a user
/**
 * add the following and followers in both sides 
 */

route.patch('/:id/follow', async (req, res, next) => {
    if (req.params.id === req.body.userId) {
        return res.status(403).json('You can not follow yourself.');
    }

    try {
        const currentUser = await User.findById(req.body.userId);
        if (!currentUser) {
            return res.status(404).jason('user not found.');
        }

        const userToFollow = await User.findById(req.params.id);
        if (!userToFollow) {
            return res.status(404).json('account not found');
        }

        if (currentUser.followings.find(following => following === req.params.id)) {
            return res.status(403).json('account followed already.');
        }

        currentUser.followings.push(req.params.id);
        userToFollow.followers.push(req.body.userId);

        await currentUser.save();
        await userToFollow.save();

        res.status(201).json('user followed successfully.');

    } catch (err) {
        console.log(err);
        return res.status(500).json(err);
    }
})

// unfollow a user

route.patch('/:id/unfollow', async (req, res, next) => {
    // user to unfollow is passed by id in the request id param
    //current user is oassed as data in the req.body
    //remove from currentuser following array 
    //remove from user to unfollow followers array
    const userToUnfollowId = req.params.id;
    const currentUserId = req.body.userId;
    if (!currentUserId) {
        return res.status(404).json('user not found');
    }
    const currentUser = await User.findById(currentUserId);
    if (!currentUser) {
        return res.status(404).json('user not found');
    }

    if (!currentUser.followings.includes(userToUnfollowId)) {
        return res.status(403).json('you do not follow this user.');
    }
    const userToUnfollow = await User.findById(userToUnfollowId);
    if (!userToUnfollow) {
        return res.status(404).json('user to unfollow not found.');
    }
    await currentUser.updateOne({ $pull: { followings: userToUnfollowId } });
    await userToUnfollow.updateOne({ $pull: { followers: currentUserId } });

    res.status(201).json('user unfollowed successfully.');
})

module.exports = route;