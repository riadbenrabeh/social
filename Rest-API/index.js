const express = require('express');
const mongoose = require('mongoose');
const helmet = require('helmet');
const morgan = require('morgan');
const dotenv = require('dotenv');
const multer = require('multer');
const path = require('path');

const userRouter = require('./routes/user');
const authRouter = require('./routes/auth');
const postsRouter = require('./routes/posts');


const app = express();

app.use('/images', express.static(path.join(__dirname, 'public/images')));
//multer configuration

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "public/images");
    },

    filename: (req, file, cb) => {
        cb(null, req.body.name);
    }
});

const upload = multer({storage: storage});

app.use('/api/upload', upload.single('file'), (req, res, next) => {
    try {
        console.log("NAME   :",req.body.name);
        res.status(200).json('File uploaded successfully.');
    } catch(err) {
        console.log(err);
    }
})



dotenv.config();

app.use(express.json());
app.use(helmet());
app.use(morgan("common"));

app.use('/api/users', userRouter);
app.use('/api/auth', authRouter);
app.use('/api/posts', postsRouter);

mongoose.connect(
    process.env.MONGODB_URI,
    { useNewUrlParser: true, useUnifiedTopology: true }
)
    .then(connection => {
        console.log('connected to database!');
    }
    );

app.listen(8000, () => {
    console.log('server is running');
})

