import  {createContext, useReducer} from 'react';
import AuthReducer from './AuthReducer';


const INITIAL_STATE = {
    user: {
        profilePicture: "/assets/images/person/5.jpg",
        coverPhoto: "",
        followers: [],
        followings: [
            "60ac3212d0676d390c94a510",
            "60afbf4adb541c1ff4e353b1",
            "60afbf67db541c1ff4e353b2",
            "60afbf77db541c1ff4e353b3",
            "60afbf9edb541c1ff4e353b4"
        ],
        isAdmin: false,
        _id: "60ac31dfd5336d21a82e8fad",
        name: "nkame1",
        from: "Madrid",
        city: "London",
        relationship: 1,
        desc: "Hey! I m new here...",
        email: "emakil@test.com",
        __v: 7
    },
    isFetching: false,
    error: false
};

export const AuthContext = createContext(INITIAL_STATE);


export const AuthContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(AuthReducer, INITIAL_STATE);
    return (
        <AuthContext.Provider value={{
            user: state.user,
            isFetching: state.isFetching,
            error: state.error,
            dispatch
        }}>
            {children}
        </AuthContext.Provider>
    )
}