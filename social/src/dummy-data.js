export const Users = [
    {
        id: 1,
        profilePhotoPath: 'assets/images/person/5.jpg',
        name: 'Camilia',
    },
    {
        id: 2,
        profilePhotoPath: 'assets/images/person/1.jpg',
        name: 'Sofia',
    },
    {
        id: 3,
        profilePhotoPath: 'assets/images/person/2.jpg',
        name: 'Mike',
    },
    {
        id: 4,
        profilePhotoPath: 'assets/images/person/4.jpg',
        name: 'James',
    },
    {
        id: 5,
        profilePhotoPath: 'assets/images/person/3.jpg',
        name: 'Lucas',
    }
]

export const Posts = [
    {
        id: 1,
        desc: 'javascript is amazing, you should start learning today.',
        photo: 'assets/images/post/3.jpg',
        date: '5 min ago' ,
        userId: 1 ,
        like: 32 ,
        comment: 9
    },
    {
        id: 2,
        desc: 'a great book, i recommand it for this week!',
        photo: 'assets/images/post/3.jpg',
        date: '9 min ago' ,
        userId: 2 ,
        like: 2 ,
        comment: 1
    },
    {
        id: 3,
        desc: 'a great book, i recommand it for this week!',
        photo: 'assets/images/post/2.jpg',
        date: '9 min ago' ,
        userId: 3 ,
        like: 2 ,
        comment: 1
    }

]