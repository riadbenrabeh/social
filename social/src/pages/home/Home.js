import './Home.css';

import TopBar from '../../components/TopBar/TopBar';
import Sidebar from '../../components/sidebar/Sidebar';
import Rightbar from '../../components/rightbar/RightBar';
import Feed from '../../components/feed/Feed';


const Home = () => {
    return (
        <div className='home'>
            <TopBar></TopBar>
            <div className='home-container'>
                <Sidebar />
                <Feed />
                <Rightbar />
            </div>
        </div>
    )
}

export default Home;