import {useState, useEffect} from 'react';
import axios from 'axios';
import {useParams} from 'react-router';
import TopBar from '../../components/TopBar/TopBar';
import Sidebar from '../../components/sidebar/Sidebar';
import Rightbar from '../../components/rightbar/RightBar';
import Feed from '../../components/feed/Feed';
import AuthContext from '../../context/AuthContext';
import './Profile.css';

export default function Profile() {
    const PF = process.env.REACT_APP_PUBLIC_FOLDER;
    const userName = useParams().userName;
    const [user, setUser] = useState({});
    console.log(userName);

    useEffect(()=>{
        const fetchUser = async () => {
            const response =  userName? await axios.get(`/users?userName=${userName}`) : await axios.get('/users/60ac31dfd5336d21a82e8fad');
            setUser(response.data);
        };
        fetchUser()
    }, [userName]);

    return (
        <>
            <TopBar></TopBar>
            <div className='profile'>
                <Sidebar />
                <div className="profile-right">
                    <div className="profile-right__top">
                        <div className="profile-cover">
                            <img src={user.coverPicture || '/assets/images/cover/default-cover.jpg'} alt="" className="profile-cover__image" />
                            <img src={`${PF}${user.profilePicture}`} alt="" className="profile-user__image" />
                        </div>
                        <div className="profile-info">
                            <h4 className="profile-info__name">{user.name}</h4>
                            <span className="profile-info__desc">{user.desc}</span>
                        </div>
                    </div>
                    <div className="profile-right__bottom">
                        <Feed userName={userName}/>
                        <Rightbar user={user} />
                    </div>
                </div>
            </div>
        </>
    )
}
