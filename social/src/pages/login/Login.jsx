import './Login.css';
import { useRef, useContext } from 'react';
import { CircularProgress } from '@material-ui/core';
import { AuthContext } from '../../context/AuthContext';
import { loginCall } from '../../apiCalls';

export default function Login() {
    const { user, isFetching, error, dispatch } = useContext(AuthContext);
    const email = useRef();
    const password = useRef();
    const loginHandler = (event) => {
        event.preventDefault();
        loginCall({ email: email.current.value, password: password.current.value }, dispatch);
    }

    console.log(user);
    return (
        <div className="login">
            <div className="login-wrapper">
                <div className="login-left">
                    <h3 className="login-logo">Social</h3>
                    <span className="login-desc">
                        connect with friends and all the world around you
                    </span>
                </div>
                <div className="login-right">
                    <form className="login-box" onSubmit={loginHandler}>
                        <input
                            type="email"
                            ref={email}
                            placeholder="email@example.com"
                            className="login-input" />
                        <input
                            type="password"
                            ref={password}
                            minLength="6"
                            placeholder="password"
                            className="login-input" />
                        <button className="login-button" disabled={isFetching}>{isFetching ? <CircularProgress size="20px" color="white"/> : 'Login'}</button>
                        <span className="login-forgot">Forgot password?</span>
                        <hr className="login-hr" />
                        <button className="login-register-button" disabled={isFetching}>Create new account</button>
                    </form>
                </div>
            </div>

        </div>
    )
}
