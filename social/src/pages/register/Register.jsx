import { useRef } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './Register.css';

export default function Register() {
    const history = useHistory();
    const email = useRef();
    const name = useRef();
    const password = useRef();
    const passwordAgain = useRef();

    const submitHandler = async (event) => {
        event.preventDefault();
        if (password.current.value !== passwordAgain.current.value) {
            password.current.setCustomValidity("password doesn't match.");
            return;
        } else {
            const user = {
                email: email.current.value,
                password: password.current.value,
                name: name.current.value
            };

            try {
                const response = await axios.post('auth/register', user);
                history.push('/login');
            } catch (err) {
                console.log(err);
            }
        }

    }
    return (
        <div className="login">
            <div className="login-wrapper">
                <div className="login-left">
                    <h3 className="login-logo">yousay</h3>
                    <span className="login-desc">
                        connect with friends and all the world around you
                    </span>
                </div>
                <div className="login-right">
                    <form className="login-box" onSubmit={submitHandler}>
                        <input
                            type="text"
                            placeholder="User name"
                            className="login-input"
                            ref={name} />
                        <input
                            type="email"
                            placeholder="Email"
                            className="login-input"
                            ref={email} />
                        <input
                            type="password"
                            placeholder="password"
                            className="login-input"
                            ref={password} />
                        <input
                            type="password"
                            placeholder="password again"
                            className="login-input"
                            ref={passwordAgain} />
                        <button type="submit" className="login-register-button">Signup</button>
                        <hr className="login-hr" />
                        <span className="login-forgot">Login to existing account</span>
                    </form>
                </div>
            </div>

        </div>
    )
}
