import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {useContext} from 'react';

import Profile from './pages/profile/Profile';
import Login from './pages/login/Login';
import Register from './pages/register/Register';
import Home from './pages/home/Home';
import { AuthContext } from './context/AuthContext';

function App() {
  const {user} = useContext(AuthContext)
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          {user && <Home /> || <Login/>}
        </Route>
        <Route  path="/profile/:userName">
          {user && <Profile /> || <Login/>}

        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/signup">
          <Register />

        </Route>
      </Switch>

    </BrowserRouter>
  );
}

export default App;
