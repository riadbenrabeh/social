import './Online.css';

export default function Online({onlineFriend}) {
    return (
        <li className="right-bar__friend">
            <div className="right-bar__profile-image-container">
                <img src={onlineFriend.profilePhotoPath} alt="" className="right-bar_profile-image" />
                <div className="friend-online"></div>
            </div>
            <span className="right-bar__user-name">{onlineFriend.name}</span>
        </li>
    )
}
