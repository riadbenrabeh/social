import { useState, useEffect, useContext } from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import { Cake, Add, Remove } from '@material-ui/icons';
import './RightBar.css';
import Online from '../online/Online';
import { AuthContext } from '../../context/AuthContext';
import { Users } from '../../dummy-data';

export default function RightBar({ user }) {
    const PF = process.env.REACT_APP_PUBLIC_FOLDER;
    const { user: currentUser } = useContext(AuthContext);
    const [friends, setFriends] = useState([]);

    useEffect(() => {
        const fetchFriends = async () => {
            try {
                const response = await axios.get(`/users/friends/${currentUser._id}`);
                const friendsList = response.data;
                setFriends(friendsList);

            } catch (err) {
                console.log(err);
            }
        };
        fetchFriends();
    }, [currentUser])

    const HomeRightBar = () => {
        return (
            <>

                <div className="birthday-container">
                    <Cake />
                    <span className="birthday-text">
                        <b>Paola Fernard</b> and <b>3 others</b>  have birthday tomorrow
                    </span>
                </div>

                <img src="/assets/images/ad/1.png" alt="" className="ad-image" />
                <h4 className="right-bar__title">Online friends</h4>
                <ul className="right-bar__friend-list">
                    {Users.map(user => <Online onlineFriend={user} key={user.id} />)}
                </ul>
            </>
        )
    }

    const ProfileRightBar = () => {
        const [isFollowed, setIsFollowed] = useState(false);
        const [followClicked, setFollowClicked] = useState(false);

        useEffect(() => {
            const fetchIsFollowing = async () => {
                try {
                    const response = await axios.get(`/users/${currentUser._id}`);
                    const followings = response.data.followings;
                    setIsFollowed(followings.includes(user._id.toString()));
                    console.log(followings);
                } catch (err) {
                    console.log(err);
                }
            };

            fetchIsFollowing();
        }, [followClicked]);

        const followHandler = async () => {
            const followAction = isFollowed? 'unfollow': 'follow';
            const response = await axios.patch(`/users/${user._id}/${followAction}`, { userId: currentUser });
            console.log(followClicked);
            setFollowClicked(!followClicked);
        }

        return (
            <>
                <button className="follow-button" onClick={followHandler}>
                {isFollowed ?<Remove htmlColor="inherit"/> : <Add htmlColor="inherit" />}
                    <span className="follow-text">{isFollowed ? 'Unfollow' : 'Follow'}</span>
                </button>
                <h4 className="right-bar__title">User information</h4>
                <div className="right-bar__info">
                    <div className="right-bar__info-item">
                        <span className="right-bar__info-key">City: </span>
                        <span className="right-bar__info-value">{user.city}</span>
                    </div>
                    <div className="right-bar__info-item">
                        <span className="right-bar__info-key">From:</span>
                        <span className="right-bar__info-value">{user.from}</span>
                    </div>
                    <div className="right-bar__info-item">
                        <span className="right-bar__info-key">relationship:</span>
                        <span className="right-bar__info-value">{user.relationship}</span>
                    </div>
                </div>
                <h4 className="right-bar__title">User friends</h4>
                <div className="right-bar__followings">
                    {friends.map(friend => {
                        return (
                            <Link to={`/profile/${friend.name}`}>
                            <div className="right-bar__following">
                                <img src={`${PF}${friend.profilePicture}`} alt="" className="right-bar__following-image" />
                                <span className="right-bar__following-name">{friend.name}</span>
                            </div>
                            </Link>
                        )
                    })}

                </div>
            </>)

    }
    return (
        <div className="right-bar">

            <div className="right-bar-wrapper">
                {user ? <ProfileRightBar /> : <HomeRightBar />}
            </div>


        </div>
    )
}
