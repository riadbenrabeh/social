import { useContext, useState, useRef, useEffect } from 'react';
import { EmojiEmotions, PermMedia, Label, Room } from '@material-ui/icons';
import axios from 'axios';

import { AuthContext } from '../../context/AuthContext';
import './Share.css';

export default function Share() {
    const { user } = useContext(AuthContext);
    const [file, setFile] = useState(null);
    
    const desc = useRef();
    const PF = process.env.REACT_APP_PUBLIC_FOLDER;


    const submitHandler = async (e) => {
        e.preventDefault();
        const newPost = {
            userId: user._id,
            desc: desc.current.value
        }

        if (file) {
            const data = new FormData();
            const fileName = Date.now() + file.name;
            data.append("name", fileName);
            data.append("file", file);
            newPost.image = fileName;
            try {
                await axios.post("/upload", data)
            } catch (err) {
                console.log(err)
            }
        }
        try {
            await axios.post('/posts', newPost);
        } catch (err) {
            console.log(err);
        }

    }

    return (
        <div className="share">
            <div className='share-wrapper'>
                <div className="share-top">
                    <img src={`${PF}/person/5.jpg`} alt="" className="share-profile-image" />
                    <input
                        placeholder={`what's in your mind ${user.name}?`}
                        type="text"
                        ref={desc}
                        className="share-input" />
                </div>
                <hr className="share-hr" />
                <form className="share-bottom" onSubmit={submitHandler}>
                    <div className="share-options">
                        <div className="share-option">
                            <label htmlFor="file">
                                <PermMedia htmlColor="blue" className="share-icon" />
                            </label>
                            <span className="share-option-text">Photo or Video</span>
                            <input
                                style={{ display: 'none' }}
                                type="file"
                                id="file"
                                accept=".jpg,.png,.jpeg"
                                onChange={(e) => setFile(e.target.files[0])} />
                        </div>
                        <div className="share-option">
                            <Label htmlColor="tomato" className="share-icon" />
                            <span className="share-option-text">Tag</span>
                        </div>
                        <div className="share-option">
                            <EmojiEmotions htmlColor='goldenrod' className="share-icon" />
                            <span className="share-option-text">Feelings</span>
                        </div>
                        <div className="share-option">
                            <Room htmlColor='green' className="share-icon" />
                            <span className="share-option-text">Location</span>
                        </div>
                    </div>
                    <button className="share-button">Share</button>
                </form>

            </div>
        </div>
    )
}
