import { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import './Feed.css';
import Share from '../share/Share';
import Post from '../post/Post';
import {AuthContext} from '../../context/AuthContext';

export default function Feed({ userName }) {
    const {user} = useContext(AuthContext);
    const [posts, setPosts] = useState([]);
    console.log(userName);

    useEffect(() => {
        const fetchPosts = async () => {
            const response = userName
                ? await axios.get(`/posts/profile/${userName}`)
                : await axios.get(`/posts/timeline/${user._id}`);
            setPosts(response.data.sort((p1, p2) => {
                return new Date(p1.createdAt) - new Date(p2.createdAt)
            }));
        };
        fetchPosts();
    }, [userName]);
    return (
        <div className='feed'>
            <div className="feed-wrapper">
            {user.name === userName && <Share />}
            {posts.map(post => <Post post={post} key={post._id} />)}

            </div>

        </div>
    )
}
