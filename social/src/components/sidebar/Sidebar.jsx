import  './Sidebar.css';

import {RssFeed, Message, PlayCircleFilledOutlined, Group, Bookmark, HelpOutline, WorkOutline, Event, School} from "@material-ui/icons"
import CloseFriend from '../closeFriend/CloseFriend';
import {Users} from '../../dummy-data';

const Sidebar = () => {
    return(
        <div className='sidebar'>
            <div className="sidebar-wrapper">
                <ul className="sidebar-list">
                    <li className="sidebar-item">
                        <RssFeed className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">Feed</span>

                    </li>
                    <li className="sidebar-item">
                        <Message className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">Chats</span>
                    </li>
                    <li className="sidebar-item">
                        <PlayCircleFilledOutlined className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">Videos</span>
                    </li>
                    <li className="sidebar-item">
                        <Group className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">Groups</span>
                    </li>
                    <li className="sidebar-item">
                        <Bookmark className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">BookMarks</span>
                    </li>
                    <li className="sidebar-item">
                        <HelpOutline className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">Questions</span>
                    </li>
                    <li className="sidebar-item">
                        <WorkOutline className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">Jobs</span>
                    </li>
                    <li className="sidebar-item">
                        <Event className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">Events</span>

                    </li>
                    <li className="sidebar-item">
                        <School className="sidebar-item__icon"/>
                        <span className="sidebar-item__text">Courses</span>

                    </li>
                </ul>
                <button className="sidebar-button">Show more</button>
                <hr className='sidebar-hr'/>
                <ul className="sidebar-friend-list">
                    {Users.map(user => <CloseFriend user = {user}/>)}
                </ul>
            </div>
        </div>
    )
}

export default Sidebar;