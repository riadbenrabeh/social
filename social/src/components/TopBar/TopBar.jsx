import {useContext} from "react";

import {Link} from 'react-router-dom';
import {Person, Search, Notifications, MessageOutlined} from '@material-ui/icons';
import {AuthContext} from '../../context/AuthContext';
import './TopBar.css';

const TopBar = () => {
    const {user} = useContext(AuthContext);

    return (
        <div className="topbar-container">
            <div className="topbar-left">
                <Link  to='/' className="logo"> Social</Link>
            </div>
            <div className="topbar-center">
                <div className="search-bar">
                    <Search className="search-icon"/>
                    <input type="text" placeholder='search for friend, post, video' className="search-input" />
                </div>
            </div>
            <div className="topbar-right">
                <div className="topbar-links">
                    <span className="topbar-link">Home</span>
                    <span className="topbar-link">TimeLine</span>
                </div>
                <div className="topbar-icons">
                    <div className="topbar-icon-item">
                        <Person className="topbar-icon"/>
                        <span className="topbar-icon-badge">1</span>
                    </div>
                    <div className="topbar-icon-item">
                        <Notifications className="topbar-icon"/>
                        <span className="topbar-icon-badge">1</span>
                    </div>
                    <div className="topbar-icon-item">
                        <MessageOutlined className="topbar-icon"/>
                        <span className="topbar-icon-badge">2</span>
                    </div>
                </div>
                <Link to={`/profile/${user.name}`}>
                
                <img src={ user.profilePicture || "/assets/images/person/user-avatar.png"} alt="" className="topbar-image" />
                </Link>
            </div>
        </div>
    )
}

export default TopBar;