import { Favorite, MoreVert, ThumbUpAlt } from '@material-ui/icons';
import { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { format } from 'timeago.js';
import { Link } from 'react-router-dom';
import './Post.css';
import {AuthContext} from '../../context/AuthContext';

const PF = process.env.REACT_APP_PUBLIC_FOLDER;


export default function Post({ post }) {

    const [likes, setLikes] = useState(post.likes.length);
    const [isLiked, setIsLiked] = useState(false);
    const [user, setUser] = useState({});
    const {user: currentUser} = useContext(AuthContext);

    useEffect(()=> {
        setIsLiked(post.likes.includes(currentUser._id));
    }, [post.likes, currentUser]);


    const onLikeHandler = async () => {
        try{
            const response = await axios.put(`posts/${post._id}/like`, {userId: currentUser._id});
            setLikes(isLiked ? likes - 1 : likes + 1);
            setIsLiked(!isLiked);
        } catch(err) {
            console.log(err);
        }

    };

    //fetch the user from post.userId

    useEffect(() => {
        const fetchUser = async () => {
            const fetchedUser = await axios.get(`/users/${post.userId}`);
            // console.log(fetchedUser);
            setUser(fetchedUser.data);
        };
        fetchUser();
    }, [post.userId])

    return (
        <div className='post'>
            <div className="post-wrapper">
                <div className="post-top">
                    <div className="post-top__left">

                        <Link to={`/profile/${user.name}`}>
                            <img
                                src={`${PF}${user.profilePicture}` || `/person/user-avatar.png`}
                                alt=""
                                className="post-profile-image" />
                        </Link>
                        <span className="post-profile-user-name">{user.name}</span>
                        <span className="post-date">{format(post.createdAt)}</span>
                    </div>
                    <div className="post-top__right">
                        <MoreVert />
                    </div>
                </div>
                <div className="post-center">
                    <span className="post-text">{post.desc}</span>
                    <img src={post.image} alt="" className="post-image" />
                </div>
                <div className="post-bottom">
                    <div className="post-bottom__left">
                        <ThumbUpAlt htmlColor='#338BFF' className='post-icon-like' onClick={onLikeHandler} />
                        <Favorite htmlColor='#FF3355' className='post-icon-like' onClick={onLikeHandler} />
                        <span className="post-like-counter">{likes} people like it</span>
                    </div>
                    <div className="post-bottom__right">
                        <span className="post-comment-counter">{post.comment} Comments</span>
                    </div>
                </div>
            </div>
        </div>
    )
}
