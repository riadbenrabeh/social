import './CloseFriend.css';

export default function CloseFriend({ user }) {
    return (
        <li className="sidebar-friend">
            <img src={user.profilePhotoPath} alt="" className="sidebar-friend__image" />
            <span className="sidebar-friend__name">{user.name}</span>
        </li>
    )
}
